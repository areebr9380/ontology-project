<?php session_start(); 
include "config.php";
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<?php
			include("includes/head.inc.php");
		?>
</head>

<body>
			<!-- start header -->
				<div id="header">
					<div id="menu">
						<?php
							include("includes/menu.inc.php");
						?>
					</div>
				</div>

				<div id="logo-wrap">
					<div id="logo">
							<?php
								include("includes/logo.inc.php");
							?>
					</div>
				</div>
			<!-- end header -->
			
			<!-- start page -->

				<div id="page">
						<!-- start content -->
				
							<div id="content">
					
								<div class="post">
									<h1 class="title">Add New Journal.</h1>
						
									<div class="entry">
									<br><br>
										<?php
											if(isset($_GET['error']))
											{
												echo '<font color="red">'.$_GET['error'].'</font>';
												echo '<br><br>';
											}
											
											if(isset($_GET['ok']))
											{
												echo '<font color="blue">You are successfully Registered..</font>';
												echo '<br><br>';
											}
										
										?>
									<?php
														include('config.php');

															if(isset($_POST['btnSubmit'])){
																
																if (true) {
																$textName=$_POST['textName'];
																$textMail=$_POST['textMail'];
																$textQualification=$_POST['textQualification'];
																$textInstitute=$_POST['textInstitute'];
																$textCountry=$_POST['textCountry'];
																$textCity=$_POST['textCity'];
																$textContact=$_POST['textContact'];
																$textAddress=$_POST['textAddress'];
																
																$query="INSERT INTO `journal` (`id`, `title`, `issue`, `publisherNmae`, `publisherWeb`, `publisherCountry`, `publisherCity`, `contact`, `address`) VALUES (null, '$textName', '$textMail', '$textQualification', '$textInstitute', '$textCountry', '$textCity', '0', '$textAddress');";
																$cmd=mysqli_query($link,$query);

																// 	$execute=mysqli_query($link,$query);
																	print "<div><li style='color:red;'>Journal Registered Successfully....</div><br>";
																}else{
																	print "<div><li style='color:red;'>Please type same Password....</div><br>";
																}

														}

														?>
										<table>
											<form method="POST">
												<tr>
													<td><b>Title Name<span style="color:red;"> *</span> :</b>&nbsp;&nbsp;</td>
													<td><input type='text' size="30" maxlength="30" required="true" placeholder="e.g Smith John" name='textName'></td>
												
												</tr>
																							
												<tr>
													<td><b>ISSN#<span style="color:red;"> *</span> :</b>&nbsp;&nbsp;</td>
													<td><input type='text' name='textMail' required="true" placeholder="john@xyz.com" size="30"></td>
													
												</tr>
											
												<tr>
													<td><b>Publisher Name<span style="color:red;"> *</span> :</b>&nbsp;&nbsp;</td>
													<td><input type='text' name='textQualification' required="true" placeholder="Bachelor of Computer Science" size="30"></td>
													
												</tr>
												<tr>
													<td><b>Publisher Web<span style="color:red;"> *</span> :</b>&nbsp;&nbsp;</td>
													<td><input type='text' name='textInstitute' required="true" placeholder="Oxford University" size="30"></td>
													
												</tr>
												<tr>
													<td><b>Country<span style="color:red;"> *</span> :</b>&nbsp;&nbsp;</td>
													<td><input type='text' name='textCountry' required="true" placeholder="Pakistan" size="30"></td>
												</tr>
												
												<tr>
													<td><b>City<span style="color:red;"> *</span> :</b>&nbsp;&nbsp;</td>
													<td><input type='text' name='textCity' required="true" placeholder="Karachi" size="30"></td>
												</tr>

												<tr>
													<td><b>Contact No<span style="color:red;"> *</span> :</b>&nbsp;&nbsp;</td>
													<td><input type='text' name='textContact' required="true" placeholder="0321-1234567" size="30"></td>
													
												</tr>
												<tr>
													<td><b>Address<span style="color:red;"> *</span> :</b>&nbsp;&nbsp;</td>
													<td><textarea style="height: 100px !important; width: 225px !important;" required="true" name="textAddress"></textarea></td>
													
												</tr>
												

												<tr>
													<td colspan='2' align='right'>
														<input type='submit' value="Submit" name="btnSubmit">
													</td>
												</tr>
											</form>
										</table>

									</div>
									
								</div>
					
					
							</div>
				
						<!-- end content -->
						
						<!-- start sidebar -->
						<div id="sidebar">
								<?php
									include("includes/search.inc.php");
								?>
						</div>
						<!-- end sidebar -->
					<div style="clear: both;">&nbsp;</div>
				</div>
			<!-- end page -->
			
			<!-- start footer -->
			<div id="footer">
						<?php
							include("includes/footer.inc.php");
						?>
			</div>
			<!-- end footer -->
</body>
</html>
