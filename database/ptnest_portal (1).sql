-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 08, 2020 at 01:39 AM
-- Server version: 10.0.38-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ptnest_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(20) NOT NULL,
  `name` text NOT NULL,
  `category` varchar(50) NOT NULL,
  `sub_category` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `author_id` int(20) NOT NULL,
  `date_of_published` varchar(255) NOT NULL,
  `jornal_id` int(11) NOT NULL,
  `status` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `name`, `category`, `sub_category`, `description`, `author_id`, `date_of_published`, `jornal_id`, `status`) VALUES
(1, 'Research_article_Dev', '19', '26', '', 1, '2018-11-20', 0, 1),
(2, 'Test_articles', '17', '19', '', 1, '2018-11-20', 0, 1),
(2406, 'Service_Worker_Burnout_and_Turnover_Intentions__Roles_of_Person_Job_Fit__Servant_Leadership_and_Customer_Orientation', '410', '22447', '', 2397, '0', 1, 1),
(2407, 'The_Impact_of_Servant_Leadership_on_Hotel_Employees_Servant_Behavior', '410', '22447', '', 2398, '0', 2, 1),
(2408, 'Servant_leadership_in_Italy_and_its_relation_to_organizational_variables', '410', '22447', '', 2399, '0', 3, 1),
(2409, 'The_Effects_of_Servant_Leader_ship_Behaviours_of_School_Principals_on_Teachers_Job_Satisfaction', '410', '22447', '', 2400, '0', 4, 1),
(2410, 'Linking_servant_leadership_to_individual_performance__Differentiating_the_mediating_role_of_autonomy__competence_and_relatedness_need_satisfaction', '410', '22447', '', 2401, '0', 5, 1),
(2411, 'Servant_leadership_versus_transformational_leadership_in_voluntary_service_organizations', '410', '22447', '', 2402, '0', 6, 1),
(2412, 'Work_family_effects_of_servants_leadership__The_roles_of_emotional_Exhaustion_and_Personal_Learning', '410', '22447', '', 2403, '0', 7, 1),
(2413, 'Servant_Leadership_and_Serving_Culture__Influence_on_Individual_and_Unit_Performance', '410', '22447', '', 2404, '0', 8, 1),
(2414, 'The_impact_of_servant_leadershipand_subordinates_organizationaltenure_on_trust_in_leader_and_attitudes', '410', '22447', '', 2405, '0', 9, 1),
(2415, 'Impact_of_Transformational_and_Servant_Leadership_on_Organizational_Performance__A_Comparative_Analysis', '410', '22447', '', 2406, '0', 10, 1),
(2416, 'How_Servent_leadership_influences_organizational_citizenship_behaviour___The_role_of_LMX__and_procative_personality', '410', '22447', '', 2407, '0', 11, 1),
(2417, 'Servant_leadership_and_employee_outcomes__the_moderating_role_of_subordinates_motives', '410', '22447', '', 2408, '0', 12, 1),
(2418, 'Does_Servent_leadership_foster_creativity_and_innovation_Question_mark_a_multi_level_mediation_study_of_identfication_and_prototypicality', '410', '22447', '', 2409, '0', 13, 1),
(2419, 'Servant_leadership_and_commitment_to_change__the_mediating_role_of_justice_and_optimism', '410', '22447', '', 2410, '0', 14, 1),
(2420, 'The_role_of_brand_credibility_in_predicting_consumers__behavioural_intentions_in_luxury_restaurants', '410', '22448', '', 2411, '0', 15, 1),
(2421, 'The_effect_of_brand_credibility_on_consumers__brand_purchase_intention_in_emerging_economies__The_moderating_role_of_brand_awareness_and_brand_image', '410', '22448', '', 2412, '0', 16, 1),
(2422, 'The_Differential_Roles_of_brand_credibility_and_brand_prestige_in_consumer_brand_choice', '410', '22448', '', 2413, '0', 17, 1),
(2423, 'The_role_of_brand_credibility_in_predicting_consumers__behavioural_intentions_in_luxury_restaurants', '410', '22448', '', 2414, '0', 18, 1),
(2424, 'The_effect_of_brand_credibility_on_consumers__brand_purchase_intention_in_emerging_economies__The_moderating_role_of_brand_awareness_and_brand_image', '410', '22448', '', 2415, '0', 19, 1),
(2425, 'The_Differential_Roles_of_brand_credibility_and_brand_prestige_in_consumer_brand_choice', '410', '22448', '', 2416, '0', 20, 1),
(2426, 'Brand_credibility__customer_loyalty_and_the_role_of_religious_orientation', '410', '22448', '', 2417, '0', 21, 1),
(2427, 'A_critical_model_of_brand_experience_consequences', '410', '22448', '', 2418, '0', 22, 1),
(2428, 'Examining_the_role_of_advertising_and_sales_promotions_in_brand_equity_creation', '410', '22448', '', 2419, '0', 23, 1),
(2429, 'How_does_brand_innovativeness_affect_brand_loyalty_', '410', '22448', '', 2420, '0', 24, 1),
(2430, 'The_impact_of_brand_credibility_on_consumer_price_sensitivity', '410', '22448', '', 2421, '0', 25, 1),
(2431, 'Brand_credibility__Brand_Consideration__and_Choice', '410', '22448', '', 2422, '0', 26, 1),
(2432, 'Does_country_of_origin_matter_in_the_relationship_between_brand_personality_and_purchase_intention_in_emerging_economies_', '410', '22448', '', 2423, '0', 27, 1),
(2433, 'Brands_as_Signals__A_Cross_Country_Validation_study', '410', '22448', '', 2424, '0', 28, 1),
(2434, 'A_Study_on_Effect_of_Brand_Credibility_on_Word_of_Mouth__With_Reference_to_Internet_Service_Providers_in_Malaysia', '410', '22448', '', 2425, '0', 29, 1),
(2435, 'Customer_satisfaction_with_services__putting_perceived_value_into_the_equation', '410', '22448', '', 2426, '0', 30, 1),
(2436, 'Customer_repurchase_intention', '410', '22448', '', 2427, '0', 31, 1),
(2437, 'Roles_of_Brand_Value_Perception_in_the_Development_of_Brand_Credibility_and_Brand_prestige', '410', '22448', '', 2428, '0', 32, 1);

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `id` int(20) NOT NULL,
  `full_name` text NOT NULL,
  `gender` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `institute` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `full_name`, `gender`, `email`, `contact`, `address`, `qualification`, `institute`, `country`, `city`, `password`, `status`) VALUES
(1, 'Areeb_Ahmed_Khan', 'male', '', '03042956400', 'Gulistan-e-Johar Karachi', 'B.Sc in computer Engineering ', 'Sir Syed University', 'Pakistan', 'Karachi', 'Asdf@1234', 1),
(2, 'Arsalan_khan_awan', 'male', 'arsalan123@gmail.com', '03042394400', 'Korangi Karachi', 'B.Sc in computer Sciences', 'ApTech', 'Pakistan', 'Karachi', 'Asdf@1234', 1),
(3, 'Naehyun_Paul_JinaComa_Seungwon_Shawn_LeebComa_JidashHyun_Junc', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2397, 'Emin_Babakus___Ugur_Yavas___Nicholas_J._Ashill', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2398, 'Long_Zeng_Wu__Eliza_Ching_Yick_Tse__Pingping_Fu__Ho_Kwong_Kwan___Jun_Liu', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2399, 'Andrea_Bobbio__Dirk_Van_Dierendonck___Anna_Maria_Manganelli', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2400, 'AYusuf_Cerit', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2401, 'Amjad_Shamim____Muhammad_Mohsin_Butt', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2402, 'Sherry_K._Schneider___Winnette_M._George', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2403, 'Guiyao_Tang__Ho_Kwong_Kwan__Deyuan_Zhang___Zhou_Zhu', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2404, 'ROBERT_C._LIDEN__SANDY_J._WAYNE__CHENWEI_LIAO___JEREMY_D._MEUSER', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2405, 'Simon_C.H._Chan___Wai_ming_Mak', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2406, 'Ali_Iftikhar_Choudhary__Syed_Azeem_Akhtar___Arshad_Zaheer', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2407, 'A._Newman__G._Schwarz__B._Cooper___S._Sendjaya', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2408, 'Magda_Donia__Usman_Raja__Alexandra_Panaccio___Zheni_Wang', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2409, 'Diah_Tuhfat_Yoshida__Sen_Sendjaya__Giles_Hirst___Brian_Cooper', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2410, 'Marjolein_Kool___Dirk_van_Dierendonck', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2411, 'Naehyun_(Paul)_Jina__Seungwon_(Shawn)_Leeb__Ji_Hyun_Junc', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2412, 'Xuehua_Wang____Zhilin_Yang', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2413, 'Tae_Hyun_Baek__Jooyoung_Kim____Jay_Hyunjae_Yu', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2414, 'Naehyun_(Paul)_Jina__Seungwon_(Shawn)_Leeb__Ji_Hyun_Junc', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2415, 'Xuehua_Wang____Zhilin_Yang', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2416, 'Tae_Hyun_Baek__Jooyoung_Kim____Jay_Hyunjae_Yu', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2417, 'Abdullah_Alam__M._Usman_Arshad____Sayyed_Adnan_Shabbir', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2418, 'Amjad_Shamim____Muhammad_Mohsin_Butt', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2419, 'Isabel_Buil_Leslie_de_Chernatony____Eva_Mart_nez', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2420, 'Ravi_Pappu____Pascale_G._Quester', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2421, 'Tu__lin_Erdem__Joffre_Swait____Jordan_Louviere', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2422, 'TU___LIN_ERDEM____JOFFRE_SWAIT', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2423, 'Xuehua_Wang____Zhilin_Yang', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2424, 'T_lin_Erdem__Joffre_Swait____Ana_Valenzuela', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2425, 'Zahra_Seyed_Ghorban____Hossein_Tahernejad', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2426, 'Gordon_H.G._McDougall____Terrence_Levesque', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2427, 'Hellier__Phillip_K.__Geursen__Gus_M.__Carr__Rodney_A._and_Rickard__John_A.', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1),
(2428, 'Chihyung_Ok__Young_Gin_Choi____Seunghyup_Seon_Hyun', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `b_id` int(4) NOT NULL,
  `b_nm` varchar(60) NOT NULL,
  `type_id` int(255) NOT NULL,
  `b_subcat` varchar(25) NOT NULL,
  `b_desc` longtext NOT NULL,
  `b_price` int(5) NOT NULL,
  `b_img` longtext NOT NULL,
  `b_pdf` longtext NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`b_id`, `b_nm`, `type_id`, `b_subcat`, `b_desc`, `b_price`, `b_img`, `b_pdf`, `added_date`, `status`) VALUES
(1, 'Java and Xml', 16, '39', 'testing ', 123, 'upload_image/41cyVNDV8CL._SX370_BO1,204,203,200_.jpg', 'upload_ebook/Java-DataStructure by Mark A. Weiss(3rd edition).pdf', '2018-05-05 00:00:00', 1),
(2, 'Digital Marketing Analytics', 3, '54', 'testing 2', 11, 'upload_image/41wV4BfeKZL._SX319_BO1,204,203,200_.jpg', 'upload_ebook/Digital Marketing Analytics.pdf', '2018-05-04 00:00:00', 1),
(3, 'Artificial Intelligence', 1, '55', 'testing 3', 11, 'upload_image/4392OS_5663_Artificial Intelligence with Python.jpg', 'upload_ebook/Artificial_Intelligence-A_Guide_to_Intelligent_Systems.pdf', '2018-05-04 00:00:00', 1),
(4, 'Finance Theory', 2, '36', 'testing 4', 11, 'upload_image/41v0ifl2CjL._SX317_BO1,204,203,200_.jpg', 'upload_ebook/Corporate Finance Theory and Practice.pdf', '2018-05-02 00:00:00', 1),
(5, 'asdasdasd', 1, '56', 'adasdasdas', 123, 'upload_image/Hydrangeas.jpg', 'upload_ebook/Cloud Computing Implementation, Management, and Security1.pdf', '2018-06-19 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(4) NOT NULL,
  `cat_nm` varchar(30) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_nm`, `status`) VALUES
(33, 'Architecture', 1),
(3, 'Forest', 0),
(4, 'Sports', 0),
(5, 'Astrology', 0),
(6, 'Business', 0),
(7, 'Economics', 0),
(8, 'Low_Books', 0),
(9, 'Tourism', 0),
(10, 'Yoga', 0),
(11, 'Religion', 1),
(12, 'Management', 1),
(13, 'Terrorism', 1),
(14, 'Tracking', 1),
(15, 'Fiction1', 1),
(16, 'Comics', 1),
(17, 'Computer_Science', 1),
(18, 'Cooking', 1),
(19, 'Science', 1),
(20, 'Compititive_Exam', 1),
(21, 'Accounting', 1),
(22, 'Marketing', 1),
(23, 'Finance', 1),
(24, 'Banking', 0),
(25, 'HR', 1),
(29, 'new_cat', 0),
(28, 'Software_Engineering', 0),
(30, 'junaid_shah', 0),
(32, 'Architecture', 0),
(34, '123', 0),
(35, 'abc', 0),
(36, 'saaaam', 0),
(37, 'test1234', 0),
(38, 'junaid_shah11', 0),
(39, 'test321', 0),
(40, 'test', 0),
(41, 'Qasim', 1),
(410, 'Buisness_managment', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `con_id` int(4) NOT NULL,
  `con_nm` varchar(25) NOT NULL,
  `con_email` varchar(35) NOT NULL,
  `con_query` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`con_id`, `con_nm`, `con_email`, `con_query`) VALUES
(5, 'owais', 'owais@ptnest.com', 'saddsad');

-- --------------------------------------------------------

--
-- Table structure for table `domain`
--

CREATE TABLE `domain` (
  `domain_id` int(255) NOT NULL,
  `domain_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `domain`
--

INSERT INTO `domain` (`domain_id`, `domain_name`, `status`) VALUES
(1, 'ptnest.com', 1),
(2, 'iobm.com', 1),
(3, 'gmail.com', 0),
(4, 'hotmail.com', 0),
(5, 'test.com', 0),
(6, 'yahoo.com', 0),
(7, 'jams.com', 0),
(8, 'jams.com', 0),
(9, 'gg', 0),
(10, 'gmail.com', 1),
(11, 'gg', 0),
(12, 'munnay.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `excel_sheet`
--

CREATE TABLE `excel_sheet` (
  `superClass` varchar(1000) NOT NULL,
  `subClass` varchar(1000) NOT NULL,
  `Objects` varchar(1000) NOT NULL,
  `Construct` varchar(1000) NOT NULL,
  `usedInstrument` varchar(1000) NOT NULL,
  `Ind` varchar(1000) NOT NULL,
  `Dep` varchar(1000) NOT NULL,
  `Publication` varchar(1000) NOT NULL,
  `ConstructIsUsedBy` varchar(1000) NOT NULL,
  `UsedConstruct` varchar(1000) NOT NULL,
  `Publications` varchar(1000) NOT NULL,
  `Title` varchar(1000) NOT NULL,
  `Journal` varchar(1000) NOT NULL,
  `Year` varchar(1000) NOT NULL,
  `AuthorName` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`file_id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11);

-- --------------------------------------------------------

--
-- Table structure for table `front_user`
--

CREATE TABLE `front_user` (
  `f_user_id` int(255) NOT NULL,
  `f_user_name` varchar(255) NOT NULL,
  `f_user_pass` varchar(255) NOT NULL,
  `f_user_gender` varchar(255) NOT NULL,
  `f_user_email` varchar(255) NOT NULL,
  `f_user_contact` varchar(255) NOT NULL,
  `f_user_city` varchar(255) NOT NULL,
  `reg_num` varchar(255) NOT NULL,
  `f_user_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `front_user`
--

INSERT INTO `front_user` (`f_user_id`, `f_user_name`, `f_user_pass`, `f_user_gender`, `f_user_email`, `f_user_contact`, `f_user_city`, `reg_num`, `f_user_status`) VALUES
(1, 'muhammad junaid', 'asd123', 'Male', 'junaid@gmail.com', '234234324', 'karachi', '', 0),
(2, 'usama', 'asd123', 'Male', 'usama@gmail.com', '232323', 'karachi', '', 0),
(3, 'arsalan', 'asd123', 'Male', 'arsalan@gmail.com', '2323', 'asfa', '290', 0),
(4, 'yasir', 'asd123', 'Male', 'yasir@gmail.com', '2323', 'karachi', '853', 0),
(5, 'waqar', 'asd123', 'Male', 'waqar@gmail.com', '343434', 'karachi', '9270', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_album`
--

CREATE TABLE `gallery_album` (
  `id` int(11) NOT NULL,
  `album_name` varchar(100) NOT NULL,
  `thubnail_image` longtext NOT NULL,
  `added_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_album`
--

INSERT INTO `gallery_album` (`id`, `album_name`, `thubnail_image`, `added_date`) VALUES
(1, 'Demo', 'gallery/240_F_57383438_yLFeiRQre9Ts7GbkfiudxD0VRPxZ2nbq.jpg', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

CREATE TABLE `gallery_images` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `image` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_images`
--

INSERT INTO `gallery_images` (`id`, `album_id`, `image`) VALUES
(1, 1, 'gallery/51JPdyH6SLL._SX321_BO1,204,203,200_.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jornal`
--

CREATE TABLE `jornal` (
  `id` int(11) NOT NULL,
  `jornal_name` varchar(255) NOT NULL,
  `jornal_year` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jornal`
--

INSERT INTO `jornal` (`id`, `jornal_name`, `jornal_year`, `status`) VALUES
(1, 'Services_Marketing_Quarterly', '2011', 1),
(2, 'Cornell_Hospitality_Quarterly', '2013', 1),
(3, 'Leadership', '2012', 1),
(4, 'Educational_Management_Administration___Leadership', '2009', 1),
(5, 'The_Leadership_Quarterly', '2015', 1),
(6, 'Leadership___Organization_Development_Journal', '2011', 1),
(7, 'Journal_of_Business_Ethics', '2015', 1),
(8, 'Academy_of_Management_Journal', '2014', 1),
(9, 'Personnel_Review', '2014', 1),
(10, 'Journal_of_Business_Ethics', '2013', 1),
(11, 'Journal_of_Business_Ethics', '2015', 1),
(12, 'European_Journal_of_Work_and_Organizational_Psychology', '2016', 1),
(13, 'Journal_of_Business_Research', '2013', 1),
(14, 'Journal_of_Organizational_Change_Management', '2012', 1),
(15, 'Anatolia', '2014', 1),
(16, 'Journal_of_Global_Marketing', '2010', 1),
(17, 'Psychology___Marketing', '2010', 1),
(18, 'Anatolia', '2014', 1),
(19, 'Journal_of_Global_Marketing', '2010', 1),
(20, 'Psychology___Marketing', '2010', 1),
(21, 'Asia_Pacific_Journal_of_Marketing_and_Logistics', '2012', 1),
(22, 'Asia_Pacific_Journal_of_Marketing_and_Logistics', '2013', 1),
(23, 'Journal_of_Business_Research', '2011', 1),
(24, 'European_Journal_of_Marketing', '2016', 1),
(25, 'International_Journal_of_Research_in_Marketing', '2002', 1),
(26, 'Journal_of_Consumer_Research', '2004', 1),
(27, 'International_Marketing_Review', '2008', 1),
(28, 'Journal_of_Marketing', '2006', 1),
(29, 'International_Journal_of_Marketing_Studies', '2012', 1),
(30, 'Journal_of_Services_Marketing', '2000', 1),
(31, 'European_Journal_of_Marketing', '2003', 1),
(32, 'International_CHRIE_Conference_Refereed_Track', '2011', 1);

-- --------------------------------------------------------

--
-- Table structure for table `old_book`
--

CREATE TABLE `old_book` (
  `b_id` int(4) NOT NULL,
  `b_nm` varchar(60) NOT NULL,
  `type_id` int(255) NOT NULL,
  `b_subcat` varchar(25) NOT NULL,
  `b_desc` longtext NOT NULL,
  `b_price` int(5) NOT NULL,
  `b_img` longtext NOT NULL,
  `b_pdf` longtext NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `old_book`
--

INSERT INTO `old_book` (`b_id`, `b_nm`, `type_id`, `b_subcat`, `b_desc`, `b_price`, `b_img`, `b_pdf`, `added_date`, `status`) VALUES
(1, 'A Dictionary of Architecture ', 2, '1', 'Containing over 5,000 entries from Aalto to ziggurat, this is the most comprehensive and up-to-date dictionary of architecture in paperback. Beautifully illustrated and written in a clear and concise style, it is an invaluable work of reference for both students of architecture and the general reader, as well as professional architects. Covers all periods of Western architectural history, from ancient times to the present day Concise biographies of leading architects, from Brunelleschi and Imhotep to Le Corbusier and Richard Rogers Over 250 illustrations specially drawn for this volume', 500, 'upload_image/ARC9.jpg', 'upload_ebook/arc.txt', '0000-00-00 00:00:00', 0),
(2, 'Elephant Kingdom : Sculptures from Indian Architecture ', 1, '1', 'Elephants occupy a special place in the life and art of India. Since ancient times, they have been treasured and pampered as the ultimate beasts of burden, venerated as the vehicles of gods and kings and even worshipped in their own right. Their legendary attributes of strength, intelligence, nobility and longevity are eulogized in myth, epic and popular literature. In the figural and decorative arts, elephants provide an enduring fascination. Elephant Kingdom traces the myriad stories and symbolisms behind India\'s much-loved animal, through its depictions in architectural sculpture. At the heart of the study is a collection of over 60 colour photographs from a diversity of antique settings-many of them in remote parts of the subcontinent. At centuries-old temples, monasteries, forts and palaces, elephants flank ceremonial entrances, enrich columns and capitals, form balustrades to stairways or stand as enigmatic sentinels of vast courtyards. Some are legendary characters in tales of dreams and salvation; others enact scenes from a faithfully observed natural history. Some transport kings and heroes into battle and the hunt; others are celestial messengers of rain, fertility and good fortune. From monumental freestanding sculptures to finely-worked narrative friezes, the warmth and energy of these depictions bear testimony to the achievements of countless anonymous artisans. ', 1000, 'upload_image/ARC8.jpg', 'upload_ebook/arc2.txt', '0000-00-00 00:00:00', 0),
(3, 'Close to Events-Works of Bikash Bhattacharjee', 2, '2', 'Launching his creative career in the late ?50s, Bikash Bhattacharjee stood out among his contemporaries by making hard-edged chiseled realism the core appeal of his canvases when realism or naturalism of every shade was considered a retrograde trend. Bikash?s strengths were his exceptional technical mastery and his power to charge the tangible appearance of the surface with the reality of the depth beneath. He was admired not merely for the near-illusionist evocation of realistic details, but for the obvious or subtle distortions in his imagery as a key to their complex multi-layered meanings. His realistic idiom is fascinatingly robust and compulsive, laced with rich irony, strong-veined allegory and lush visual metaphors. His portrait-based images enact the artist?s own experience of our time with all its dark social and moral tones and textures. Close to Events: Works of Bikash Bhattacharjee deals with Bikash?s early life in an old North Calcutta locality, the urban social ambience that shaped his creative personality and explores why he chose to remain ?close to events? and free from the dominant trends in post-Independence Indian art. It also analyses the technical and stylistic development of art with detailed exposition of some of the themes and subjects in the major series of his paintings. ', 2147483647, 'upload_image/ART6.JPG', 'upload_ebook/art1.docx', '0000-00-00 00:00:00', 0),
(4, 'A Social History of Indian Architecture ', 2, '1', 'Studies in Indian architecture have been confined to those exploring the building techniques of palaces, temples, and tombs. Little attention has traditionally been paid by scholars to the patterns and influences involved in the making of domestic residences, market places, inns, community halls, courts, and other \'lesser\' buildings. The result is the emergence of a very partial picture of what constitutes architecture in India. This volume se eks to overcome this inadequacy by examining the geographical, historical, and functional aspects of architecture in India. Looking beyond the point of view of dynasties, periods or religions, the book traces the various social and historical developments in the field. Following a multi-disciplinary approach that emphasizes sociological aspects, the volume examines in detail, settlement patterns, the architecture of individual houses and chaityas, as also structural materials used for their construction, in addition to those of palaces, funerary monuments, temples, mosques, and monasteries. While examining the planning and design orientation of peoples and architectural techniques across India, the volume pays special attention to that of western India especially Gujarat and Rajasthan. The author also addresses rural and urban patterns of setlement and the linkages between the two. He explains regional and period-specific phenomena, while also quoting from ancient accounts of towns. These vary from the typical urban and rural houses to the Muslim aristocratic residences and Rajput palaces. Based on extensive fieldwork, the author also documents family histories, lifestyles and usage of space to provide a comprehensive social history of Indian architecture. Complemented by over eighty figures including photographs, plans, and detailed diagrams, this book will interest scholars of architecture, history, sociology, and the informed lay reader', 1000, 'upload_image/ARC10.jpg', 'upload_ebook/arc3.txt', '0000-00-00 00:00:00', 0),
(5, 'new2', 1, '5', 'testingasdasdasdasdasdas\r\n', 350000000, 'upload_image/ASTRO1.jpg', 'upload_ebook/ASTR1.doc', '0000-00-00 00:00:00', 1),
(6, 'You Deserve, We Conserve A Biotechnological Approach to Wil', 2, '3', 'dsdasdasdasdaadsdassdadsa', 659, 'upload_image/forest.jpg', 'upload_ebook/forest.docx', '0000-00-00 00:00:00', 1),
(7, 'Visual Basics 2005', 0, '17', '', 120, 'upload_image/comp8.jpg', 'upload_ebook/read.pdf', '0000-00-00 00:00:00', 1),
(8, 'Java & Xml', 0, '17', '\'Complete Reference\'', 500, 'upload_image/comp9.jpg', 'upload_ebook/java.pdf', '0000-00-00 00:00:00', 1),
(9, 'Microsoft Windows Powershell Step By Step', 0, '29', '\'Learn Microsoft Windows PowerShell step by step with hands-on instruction from a leading Microsoft scripting trainer. This guide features self-paced labs, timesaving tips, and dozens of sample scripts', 295, 'upload_image/comp6.jpg', 'upload_ebook/wave.doc', '0000-00-00 00:00:00', 1),
(10, 'C# Programming', 0, '17', '\'C# is platform independent,includes namespace,garbage collection,automatic memory management', 300, 'upload_image/1861004877.jpg', 'upload_ebook/intro_C#.pdf', '0000-00-00 00:00:00', 1),
(11, 'Java Server Programming', 0, '17', '\'jsp uses  html tags and run on java platform\'', 560, 'upload_image/1861004656.jpg', 'upload_ebook/Java_2_5th-www.netbks.com.pdf', '0000-00-00 00:00:00', 1),
(12, 'Programming with Perl', 0, '17', 'Perl is programming langauge which is not comfortable to handle.', 450, 'upload_image/0596000278.jpg', 'upload_ebook/perl.docx', '0000-00-00 00:00:00', 1),
(13, 'HTML for world wide web', 0, '19', 'asdasd', 400, 'upload_image/0201354934.jpg', 'upload_ebook/html.pdf', '0000-00-00 00:00:00', 1),
(14, 'ASP Server Pages 3.0', 0, '17', 'Active server pages uses xml files ,it run on internet explorer or other browser..', 950, 'upload_image/1861003382.jpg', 'upload_ebook/asp.doc', '0000-00-00 00:00:00', 1),
(15, 'Perl and CGI', 0, '17', 'CGI for graphics purpose', 300, 'upload_image/020135358X.gif', 'upload_ebook/perl1.pdf', '0000-00-00 00:00:00', 1),
(16, 'A Biological Survey for the Nation', 0, '27', 'The National Biological Survey will produce the map we need to avoid the\\r\\neconomic and environmental \"train wrecks\" we see scattered across the country.\\r\\nNBS will provide the scientific knowledge America needs to balance the\\r\\ncompatible goals of ecosystem protection and economic progress', 450, 'upload_image/biology.gif', 'upload_ebook/g.pdf', '0000-00-00 00:00:00', 1),
(17, 'Book of Tea', 0, '23', 'The Philosophy of Tea is not mere aestheticism in the ordinary acceptance of the term, for it expresses conjointly with ethics and religion our whole point of view about man and nature. It is hygiene, for it enforces cleanliness.It represents the true spirit of Eastern democracy by making all its votaries aristocrats in taste. (from \"The Book of Tea\")', 100, 'upload_image/bookoftea.jpg', 'upload_ebook/tea.docx', '0000-00-00 00:00:00', 1),
(18, 'Coffee : Scrumptious Drinks', 0, '23', 'Every day, millions search for The Perfect Cup of Coffee in caf s, diners, and kitchens around the world. Here, coffee guru Betty Rosbottom offers easy-to-follow recipes guaranteed to please \\r\\nanyone who takes delight in sampling, sipping, and serving exquisite coffee concoctions.', 100, 'upload_image/cofee.jpg', 'upload_ebook/cofee.docx', '0000-00-00 00:00:00', 1),
(19, 'Stone Soup', 0, '24', 'A hungry traveler tricks a little old lady into cooking him soup starting with a stone.', 350, 'upload_image/stonesoup.jpg', 'upload_ebook/soup.docx', '0000-00-00 00:00:00', 1),
(20, 'Pasta Perfection', 0, '22', 'This new series will help you get back into the kitchen and experience the fun of creating sensationalmouth-watering meals thought he simplicity of easy-to-read, step-by-step ...', 100, 'upload_image/pasta1.jpg', 'upload_ebook/pasta1.docx', '0000-00-00 00:00:00', 1),
(21, 'Bhartiya Vynjano ka khajana', 0, '25', 'Vyanjan made by Sanjeev kapoor,he makes very sweet and delicious dishes', 350, 'upload_image/bhartiya.jpg', 'upload_ebook/Vyanjan.docx', '0000-00-00 00:00:00', 1),
(22, 'Descriptious du Cafeier', 0, '23', 'millions search for The Perfect Cup of Coffee in caf s, diners, and kitchens around the world. Here, coffee guru Betty Rosbottom offers easy-to-follow recipes guaranteed to please anyone who takes delight in sampling, sipping, and serving exquisite coffee concoctions.', 100, 'upload_image/cofee2.jpg', 'upload_ebook/Coffee1.docx', '0000-00-00 00:00:00', 1),
(23, 'Your Income-Tax 2010', 0, '8', 'A guide to income tax returns provides information on the most recent tax legislation, tax-filing tips, advice on how to reduce tax liabilities, helpful financial advice, and sample tax forms, worksheets,', 1000, 'upload_image/tax1.jpg', 'upload_ebook/Income Tax.pptx', '0000-00-00 00:00:00', 1),
(24, 'Your Income-Tax Professional Edition', 0, '8', 'Provides information about filing requirements, exemptions, income, deductions, tax credits, shelters, and tax law.', 120, 'upload_image/tax2.jpg', 'upload_ebook/itax2.pptx', '0000-00-00 00:00:00', 1),
(25, 'J K Lesser\'s Tax Savings in your Pocket', 0, '8', '*  Save more for your child education now!\r\n   * Increase your retirement savings\r\n   * New deductions, tax breaks, and planning tips', 300, 'upload_image/tax3.jpg', 'upload_ebook/Saving.pptx', '0000-00-00 00:00:00', 1),
(26, 'On Liberty', 0, '8', 'the liberal tradition, revered for his defense of liberal principles and expansive personal liberty.', 300, 'upload_image/li1.jpg', 'upload_ebook/lib1.docx', '0000-00-00 00:00:00', 1),
(27, 'On Liberty in Focus', 0, '8', 'his book gathers together for the first time J.S. Mill\'\'s On Liberty and a selection of importantessays by the eminent scholars Isaiah Berlin, Alan Ryan, John Rees C.L. Ten\'', 350, 'upload_image/li2.jpg', 'upload_ebook/lib2.docx', '0000-00-00 00:00:00', 1),
(28, 'Debugging Microsoft .NET 2.0 Applications', 0, '17', 'Get hands-on instruction for using the tools in Microsoft Visual Studio? 2005 to debug, tune, and test applications. This guide features practical advice and code samples for developers at all levels from a leading authority on improving code. Traditionally, tools for performance tuning, testing applications, and debugging code have been expensive, hard to learn, and difficult to use. While previous versions of Microsoft Visual Studio? have included debuggers and other code-improvement tools, Visual Studio 2005 presents developers with robust and useful tools and processes to help ensure top-quality code. In this guide, an expert on improving code, John Robbins, steps back from the expert-level information that characterized his previous debugging books to present hands-on, practical advice for working developers on how to use the debugging, testing, and tuning features in Visual Studio 2005', 699, 'upload_image/comp2.jpg', 'upload_ebook/net2005.docx', '0000-00-00 00:00:00', 1),
(29, 'The Mad, Mad World of Cricket', 0, '4', 'The funny side of the gentleman?s game?captured by a master cartoonist In India cricket is more than a game; it is a national obsession. And with a World Cup always around the corner, there is no better way to prepare for the excitement of seeing the men in blue in action than with renowned cartoonist Sudhir Dar?s creations.', 125, 'upload_image/c1.jpg', 'upload_ebook/cricket1.pptx', '0000-00-00 00:00:00', 1),
(30, 'Dream Team India: The Best World Cup Squad Ever !', 0, '4', 'Are you one of those who just knows India will win the match the moment the game starts? Do you drown yourself in cricket and cricket-related trivia every four years and dream of seeing India win the World Cup? If the answer is ?yes? to any of the above, here is the team that will bring home the Cup for you. ', 200, 'upload_image/c2.jpg', 'upload_ebook/cricket2.docx', '0000-00-00 00:00:00', 1),
(31, 'HOW TO PREPARE FOR QUANTITATIVE APTITUDE FOR CAT', 0, '33', 'More than 3000 questions categorised into three levels of difficulty - LOD1, LOD2 and LOD3 * Notes emphasising relative importance of topics in the CAT, at appropriate places in the book * Short-cut methods to aid faster solutions to problems * Five practice CAT tests (actual CAT questions based on memory)', 325, 'upload_image/COMPETITIVE8.jpg', 'upload_ebook/cat1.docx', '0000-00-00 00:00:00', 1),
(32, 'Physics', 0, '26', 'Get all you need to know with Super Reviews! Each Super Review is packed with in-depth, student-friendly topic reviews that fully explain everything about the subject.', 495, 'upload_image/p2.jpg', 'upload_ebook/pysics1.pptx', '0000-00-00 00:00:00', 1),
(33, 'Thermal Physics', 0, '26', 'The book presents a lucid and systematic exposition of the fundamental principles of Thermal Physics.', 163, 'upload_image/p6.jpg', 'upload_ebook/thermal.pptx', '0000-00-00 00:00:00', 1),
(34, 'The Rough Guide to the Earth?', 0, '2', 'From the opening and closing of oceans over millions of years to the overnight reshaping of landscapes by volcanoes, the Earth beneath our feet is constantly changing. The Rough Guide to the Earth explores all aspects of our dynamic planet, from the planet?s origins and evolution and the seasons and tides to melting ice caps, glaciers and climate change. Featuring many spectacular images and helpful diagrams, this Rough Guide provides a fascinating and accessible introduction to Earth science.', 650, 'upload_image/ART3.JPG', 'upload_ebook/Earth science.pptx', '0000-00-00 00:00:00', 1),
(35, 'A TEXTBOOK OF COST AND MANAGEMENT ACCOUNTING 8th ed.', 0, '6', ' Student friendly and examination oriented approach # Innovative, comprehensive and systematic presentation of the subject matter # Use of diagrams and exhibits to help students understand concepts easily and clearly # Around 500 solved problems and illustrations with working notes # Solved and unsolved practical questions from various university and professional examinations like BCom, MCom, CA, CS, ICWA, etc. # Objective type questions and select theory questions # Ideal for self study.', 395, 'upload_image/busi7.jpg', 'upload_ebook/cost_a_c.pptx', '0000-00-00 00:00:00', 1),
(36, 'Computer Networks, 4th Ed', 0, '20', '(38, \'Computer Networks, 4th Ed\', \'46\', \'updated, this classic bestseller, now in its fourth edition, reflects the newest and most important networking technologies with a special emphasis on wireless networking. The material on wireless networks includes detailed coverage of 802.11, wireless local loops, 2G and 3G cellular networks, BluetoothTM, WAP, i-mode, and others. It prepares students to work with wireless technologies in networks of all sizes-both local and wide area networks. There is also lots of new material on applications, including the Web, Internet radio, voice over IP, and video on demand. Finally, an entirely new chapter is devoted exclusively to security to help students deal with one of the most crucial topics in networking today. Despite a large amount of material added on wireless networks, fixed networks have not been ignored-topics covered include ADLS, Internet over cable, gigabit Ethernet, peer-to-peer networks, NAT, and MPLS. Each chapter follows a consistent approach. The author first presents the key principles-underlying hardware at the physical layer up through the top-level application layer-and then illustrates them utilizing real-world examples drawn from the Internet and wireless networks, all in Tanenbaum\'\'s classic entertaining style.', 325, 'upload_image/comp7.jpg', 'upload_ebook/SLIP and PPP.docx', '0000-00-00 00:00:00', 1),
(37, 'Investing for Beginners', 0, '6', 'Investment Risks and Rewards: How to overcome the fear of investment risk and how taking a few risks can reap long-term benefits. Your Starting Point: How to assess your investment goals. Diversification: How to allocate your money among various investment avenues for safety, steady income and capital growth. How to Pick Stocks: How to use fundamental indicators of value to pick good stocks. Investing in Bonds: Why you need bonds in your portfolio and which bonds to choose. Mutual Funds Primer: What they are and how to select the ones that suit your needs. Disinvesting: How to figure out when it\'\'s time to get out of an investment. Keeping in Touch: How to understand financial information. Keeping Track of Your Investments: Simple record-keeping tricks.', 195, 'upload_image/business.jpg', 'upload_ebook/Investment.docx', '0000-00-00 00:00:00', 1),
(38, 'Games Lawyers need to Play - Moot Court Problems ', 0, '8', 'The Raj Anand Moot Court Competition was initiated in 1998 with its focus centrally on Intellectual Property law. Over the years the scope of the Competition has widened though Intellectual Property remains the core area. One of its primary aims is to sharpen the skills of ?mooting? among aspiring lawyers. Games Lawyers Need to Play brings together the Problems and ten of the finest Memorials of the Competition. Each chapter deals with a specific year beginning with 2004 and going back to 1998. The problems deal with various aspects of Intellectual Property but are ?out of ordinary?, to enable participants to combine good quality research with creativity and originality. The book has a foreword by Judge Michael Fysh, QC, SC. The Introduction is written by Pravin Anand.', 595, 'upload_image/lawyer.jpg', 'upload_ebook/lawyer.doc', '0000-00-00 00:00:00', 1),
(39, 'An ABC of Indian Culture : A Personal Padayatra of Half a Ce', 0, '2', 'An authentic interpretation of over 400 Indian concepts and practices derived from a personal exploration of India over a period of 50 years. Arranged alphabetically, these range from key traditional ones such as \'dharma\' to more contemporary ones such as \'secularism\' and \'democracy\' to popular ones such as Indian films! \'Padayatra\' is a journey on foot and each selected concept and practice is treated as a stepping-stone in a journey to understanding what India is all about. Descriptions are based on personal experience maturing over half a century, and written in cultural essays that present the essence of the Indian tradition. Malformations of the tradition are explained but without polemics. The book is a sensitive, cultured and sophisticated introduction to India for an intelligent and serious readership, and will be invaluable also as a handy reference text for libraries, cultural exchange agencies, business orientation courses especially for those anticipating an extended interaction with India, and the like.', 595, 'upload_image/cul1.jpg', 'upload_ebook/culture1.docx', '0000-00-00 00:00:00', 1),
(40, 'HOW TO PREPARE FOR THE CAT, 2/E ', 0, '33', 'More than 3000 questions categorised into three levels of difficulty - LOD1, LOD2 and LOD3 * Notes emphasising relative importance of topics in the CAT, at appropriate places in the book * Short-cut methods to aid faster solutions to problems * Five practice CAT tests (actual CAT questions based on memory)', 499, 'upload_image/CAT.jpg', 'upload_ebook/cat2.docx', '0000-00-00 00:00:00', 1),
(41, 'Safe and Simple Steps to Fruitful Meditation', 1, '10', '', 80, 'upload_image/yoga1.jpg', 'upload_ebook/yoga1.docx', '0000-00-00 00:00:00', 1),
(42, 'STATISTICS FOR BUSINESS AND ECONOMICS', 0, '7', 'This book covers various aspects of the field of statistics in 20 chapters, making each topic relevant and useful. A unique feature of this book is the inclusion of databases to be utilized by computers and software statistical packages. Contents - Introduction ? Statistical Terms and Concepts ? Data Collection ? Data Presentation ? Data Characteristics: Descriptive Measures ? Basic Concepts of Probability ? Probability Distribution ? Sampling Distribution ? Statistical Inference: Estimation ? Hypothesis Testing I ? Hypothesis Testing II ? Hypothesis Testing III ? Hypothesis Testing IV (Comparing Several Proportions Chi Square Test) ? Hypothesis Testing V(Comparing Several Population Means) One-Way Analysis of Variance (ANOVA) ? Regression and Correlation Analysis ? Multiple Regression ? Non-Parametric Statistics ? Time Series Analysis ? Statistical Decision Making ? Statistical Quality.', 372, 'upload_image/9062999.jpg', 'upload_ebook/maths.doc', '0000-00-00 00:00:00', 1),
(43, 'Himalayan Vignettes : The Garhwal and Sikkim Treks', 0, '14', 'In the 1950s Himalayan trekking was not as popular as it is now. The network of roads deep into the Himalayas did not exist and the hills were more pristine and undeveloped.', 2000, 'upload_image/1445.jpg', 'upload_ebook/track1.txt', '0000-00-00 00:00:00', 1),
(44, 'Insight Guide Iceland', 0, '9', 'A travel series unlike any other, Insight Guides go beyond the sights and into reality.', 935, 'upload_image/t2.jpg', 'upload_ebook/tour1.txt', '0000-00-00 00:00:00', 1),
(45, 'SPIDER MAN', 0, '16', 'second 100 issues as May Mayday Parker learns that she can\'t escape her great responsibilities! Featuring the original Hobgoblin, the Black Tarantula and more! Plus: the saga of Spider-Girl! Collects Amazing Spider-Girl #0-6. ', 606, 'upload_image/comic1.jpg', 'upload_ebook/spider.doc', '0000-00-00 00:00:00', 1),
(46, 'The Missing ', 0, '15', 'The woman missing for five years. The Crime Scene Investigator who finds her. And the serial killer who wants them both dead? When Boston CSI Darby McCormick finds a raving and emaciated woman hiding at the scene of a violent kidnap, she runs a DNA search to identify the Jane Doe. The result confirms that the woman was abducted five years earlier and has somehow managed to escape from the dungeon in which she?s been caged. With a teenage couple also missing and the Jane Doe seriously ill, the clock is ticking for Darby as she hunts for the dungeon before anyone else disappears or dies. And when the FBI takes over the investigation, it becomes clear that a sadistic serial killer has been on the prowl for decades ? and is poised to strike again at any moment. A killer with links to horrors that Darby has desperately tried to bury in her past?\r\n\r\n', 240, 'upload_image/fic1.jpg', 'upload_ebook/fiction1.docx', '0000-00-00 00:00:00', 1),
(47, 'Bhagavata Purana ', 0, '11', 'Even after he has composed the awesome Mahabharata, the Maharishi Vyasa finds no peace.', 995, 'upload_image/re7.jpg', 'upload_ebook/bagvad.txt', '0000-00-00 00:00:00', 1),
(48, 'Bill and Dave: How Hewlett and Packard Built the World`s Gre', 0, '12', 'This is not a history of the Hewlett-Packard Company, or a book of business theory, or a definitive biography of William Hewlett and David Packard. I have chosen to write this book this way because of the desperate need the business world has right now for an archetype of enlightened management, enduring quality, and perpetual innovation. It is not enough to simply tell the story of Hewlett, Packard and their company. What are needed are the why? and the how?? The most momentous first meeting in modern business history took place in the unlikely setting of a bench beside a football field, between two Stanford University students in pads and helmets. A few years later, in 1938, Bill Hewlett and Dave Packard were working in a small garage in Palo Alto, California, building their first product, an audio oscillator. It was the start not only of a legendary company but also of an entire way of life in Silicon Valley?and, ultimately, of our modern digital age. Acclaimed journalist Michael S. Malone is the first to get the full story, based on unlimited and exclusive access to corporate and private archives, along with hundreds of employee interviews. He draws on new material to show how some of the most influential products of our time were invented and how a culture of innovation led HP to unparalleled success for decades. Malone also shows what was really behind the groundbreaking management philosophy??the HP way??that put people ahead of products or profits. Bill and Dave, at its heart, is a character study of two amazing men who revealed their character in how they structured their business, in the men and women they hired, and, most of all, in the power they entrusted to even the lowliest HP employee. Their story is something of a miracle?one from which we can never stop learning.', 500, 'upload_image/MANAGEMENT2.jpg', 'upload_ebook/mgmt1.pptx', '0000-00-00 00:00:00', 1),
(49, 'PAKISTAN`S DRIFT INTO EXTREMISM', 0, '13', 'The book studies the rise of religious extremism in pakistan and analyses its connection to the pakistani army policies and fluctuating US - Pakistani Relationship. It is a book which readers as well as students of Political Science and history will enjoy thoroughly.', 600, 'upload_image/terr2.jpg', 'upload_ebook/terror1.txt', '0000-00-00 00:00:00', 1),
(50, 'Learning SQL on SQL Server 2005 : The Simplest Way', 0, '18', 'Anyone who interacts with today?s modern databases needs to know SQL (Structured Query Language), the standard language for generating, manipulating, and retrieving database information. In recent years, the dramatic rise in the popularity of relational databases and multiuser databases has fueled a healthy demand for application devel?opers and others who can write SQL code efficiently and correctly. If you?re new to databases or need a SQL refresher, Learning SQL on SQL Server 2005 is an ideal step-by-step introduction to this database query tool, with everything you need for programming SQL using Microsoft?s SQL Server 2005?one of the most powerful and popular database engines used today. Plenty of books explain database theory. This guide lets you apply the theory as you learn SQL. You don?t need prior database knowledge, or even prior computer knowledge. Based on a popular university-level course designed by authors Sikha Saha Bagui and Richard Walsh Earp, Learning SQL on SQL Server 2005 starts with very simple SQL concepts, and slowly builds into more complex query development. Every topic, concept, and idea comes with examples of code and output, along with exercises to help you gain proficiency in SQL and SQL Server 2005. With this book, you?ll learn: * Beginning SQL commands, such as how and where to type an SQL query, and how to create, populate, alter, and delete tables * How to customize SQL Server 2005?s settings and about SQL Server 2005?s functions * About joins, a common database mechanism for combining tables * Query development, the use of views and other derived structures, and simple set operations * Subqueries, aggregate functions, and correlated subqueries, as well as indexes and constraints that can be added to tables in SQL Server 2005 Whether you?re a self-learner who has access to the new Microsoft database, working on SQL Server with access at your company, or a computer science student or MIS student, Learning SQL on SQL Server 2005 will get you up to speed on SQL in no time.\r\n\r\n', 350, 'upload_image/comp10.jpg', 'upload_ebook/sql1.docx', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `owlexport`
--

CREATE TABLE `owlexport` (
  `ID` int(11) NOT NULL,
  `ClassTag` text NOT NULL,
  `NamedIndividual` text NOT NULL,
  `DatatypeProperty` text NOT NULL,
  `ObjectProperty` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owlexport`
--

INSERT INTO `owlexport` (`ID`, `ClassTag`, `NamedIndividual`, `DatatypeProperty`, `ObjectProperty`) VALUES
(1, '<owl:Class rdf:about=\"&*;*\"/>', '<owl:NamedIndividual rdf:about=\"&*;*\">*\r\n        <rdf:type rdf:resource=\"&*;*\"/>\r\n*</owl:NamedIndividual>', ' <owl:DatatypeProperty rdf:about=\"&*;*\"/>', '<owl:ObjectProperty rdf:about=\"&*;*\"/>*\r\n</owl:ObjectProperty>');

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `rel_id` int(11) NOT NULL,
  `rel_name` varchar(50) NOT NULL,
  `rel_dependent` varchar(50) NOT NULL,
  `rel_independent` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`rel_id`, `rel_name`, `rel_dependent`, `rel_independent`) VALUES
(1, 'has', 'category', 'articles'),
(2, 'has_subCategory', 'subcat', 'articles'),
(3, 'writes', 'author', 'articles'),
(4, 'has_the', 'type', 'book'),
(5, 'part_of', 'subcat', 'book'),
(6, 'is_the_image_of', 'data_image', 'book'),
(7, 'is_the_printed_book_of', 'book', 'data_pdf'),
(8, 'has_A', 'type', 'old_book'),
(9, 'has_article', 'subcat', 'old_book'),
(10, 'has_type', 'user_type', 'user'),
(11, 'variable_of', 'articles', 'variables'),
(12, 'subcategory_usedIn', 'articles', 'sub_category'),
(13, 'is_category_of', 'category', 'articles');

-- --------------------------------------------------------

--
-- Table structure for table `spread_sheet_upload`
--

CREATE TABLE `spread_sheet_upload` (
  `id` int(11) NOT NULL,
  `file_name` int(11) NOT NULL,
  `file_ext` varchar(25) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spread_sheet_upload`
--

INSERT INTO `spread_sheet_upload` (`id`, `file_name`, `file_ext`, `status`) VALUES
(1, 1, 'xls', 1),
(2, 2, 'xls', 1),
(3, 3, 'xls', 1),
(4, 4, 'xls', 1),
(5, 5, 'xls', 1),
(6, 6, 'xls', 1),
(7, 7, 'xls', 1),
(8, 8, 'xls', 1),
(9, 9, 'xls', 1),
(10, 10, 'xls', 1),
(11, 11, 'xls', 1),
(12, 12, 'xls', 1),
(13, 13, 'xls', 1),
(14, 14, 'xls', 1),
(15, 15, 'xls', 1),
(16, 16, 'xls', 1),
(17, 17, 'xls', 1),
(18, 18, 'xls', 1),
(19, 19, 'xls', 1),
(20, 20, 'xls', 1),
(21, 21, 'xls', 1),
(22, 22, 'xls', 1),
(23, 23, 'xls', 1),
(24, 24, 'xls', 1),
(25, 25, 'xls', 1),
(26, 26, 'xls', 1),
(27, 27, 'xls', 1),
(28, 28, 'xls', 1),
(29, 29, 'xls', 1),
(30, 30, 'xls', 1),
(31, 31, 'xls', 1),
(32, 32, 'xls', 1),
(33, 33, 'xls', 1),
(34, 34, 'xls', 1),
(35, 35, 'xls', 1),
(36, 36, 'xls', 1),
(37, 37, 'xls', 1),
(38, 38, 'xls', 1),
(39, 39, 'xls', 1),
(40, 40, 'xls', 1),
(41, 41, 'xls', 1),
(42, 42, 'xls', 1),
(43, 43, 'xls', 1),
(44, 44, 'xls', 1),
(45, 45, 'xls', 1),
(46, 46, 'xls', 1),
(47, 47, 'xls', 1),
(48, 48, 'xls', 1),
(49, 49, 'xls', 1),
(50, 50, 'xls', 1),
(51, 51, 'xls', 1),
(52, 52, 'xls', 1),
(53, 53, 'xls', 1),
(54, 54, 'xls', 1),
(55, 55, 'xls', 1),
(56, 56, 'xls', 1),
(57, 57, 'xls', 1),
(58, 58, 'xls', 1),
(59, 59, 'xls', 1),
(60, 60, 'xls', 1),
(61, 61, 'xls', 1),
(62, 62, 'xls', 1),
(63, 63, 'xls', 1),
(64, 64, 'xls', 1),
(65, 65, 'xls', 1),
(66, 66, 'xls', 1),
(67, 67, 'xls', 1),
(68, 68, 'xls', 1),
(69, 69, 'xls', 1),
(70, 70, 'xls', 1),
(71, 71, 'xls', 1),
(72, 72, 'xls', 1),
(73, 73, 'xls', 1),
(74, 74, 'xls', 1),
(75, 75, 'xls', 1),
(76, 76, 'xls', 1),
(77, 77, 'xls', 1),
(78, 78, 'xls', 1),
(79, 79, 'xls', 1),
(80, 80, 'xls', 1),
(81, 81, 'xls', 1),
(82, 82, 'xls', 1),
(83, 83, 'xls', 1),
(84, 84, 'xls', 1),
(85, 85, 'xls', 1),
(86, 86, 'xls', 1),
(87, 87, 'xls', 1),
(88, 88, 'xls', 1),
(89, 89, 'xls', 1),
(90, 90, 'xls', 1),
(91, 91, 'xls', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subcat`
--

CREATE TABLE `subcat` (
  `subcat_id` int(4) NOT NULL,
  `parent_id` int(4) NOT NULL,
  `subcat_nm` varchar(1000) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcat`
--

INSERT INTO `subcat` (`subcat_id`, `parent_id`, `subcat_nm`, `status`) VALUES
(1, 1, 'Architecture', 1),
(2, 2, 'Art-And_Culture', 1),
(3, 3, 'Forest', 1),
(4, 4, 'Sports', 0),
(5, 5, 'Astrology', 0),
(6, 5, 'Businesses', 0),
(7, 5, 'Economies', 1),
(8, 8, 'Low_Books', 1),
(9, 9, 'Tourism', 1),
(10, 10, 'Yogaa', 1),
(11, 11, 'Religion', 0),
(12, 12, 'Management', 1),
(13, 13, 'Terrorism', 1),
(14, 14, 'Tracking', 1),
(15, 14, 'Fiction', 0),
(16, 14, 'Comics', 0),
(17, 17, 'Programming', 1),
(18, 17, 'Database', 1),
(19, 17, 'Web-Design', 1),
(20, 17, 'Networking', 1),
(22, 18, 'Pasta', 1),
(23, 18, 'Tea-Coffee', 1),
(24, 18, 'Soup-Sauce', 1),
(25, 18, 'Vegetarian_Item', 1),
(26, 19, 'Physics', 1),
(27, 19, 'Biology', 1),
(28, 19, 'Medical', 1),
(29, 17, 'O.S.', 1),
(33, 20, 'CAT', 1),
(31, 20, 'GMAT', 1),
(32, 20, 'MBA', 1),
(34, 21, 'Accounting', 1),
(35, 22, 'Marketing', 1),
(36, 23, 'Finance', 1),
(37, 24, 'Banking', 1),
(38, 25, 'Human_Resource', 0),
(39, 17, 'Software_Engineering', 1),
(40, 26, 'Networking', 1),
(41, 26, 'Software_Engineering', 1),
(42, 28, 'RDBMS', 1),
(44, 30, 'arsalan', 0),
(45, 1, 'software_design', 1),
(46, 1, 'Home_Design', 1),
(47, 0, 'new', 1),
(48, 0, 'asd', 1),
(49, 33, 'news', 1),
(50, 10, 'yogaa', 0),
(51, 10, 'yog', 0),
(52, 10, 'Yogaa', 0),
(53, 10, 'yoggf', 0),
(54, 6, 'Digital_Marketing', 1),
(55, 17, 'Artificial_Intelligence', 1),
(56, 39, 'asdasd1', 0),
(57, 33, 'home_arch', 1),
(58, 41, 'balti', 1),
(22447, 410, 'Construct_of_Servant_Leadership', 1),
(22448, 410, 'Construct_of_Predicting_Customers_Purchasing_Behavior', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testing`
--

CREATE TABLE `testing` (
  `id` int(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `file_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `testing_db`
--

CREATE TABLE `testing_db` (
  `id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL DEFAULT '0',
  `category` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testing_db`
--

INSERT INTO `testing_db` (`id`, `p_id`, `category`, `status`) VALUES
(1, 0, 'testing', 1),
(2, 0, 'google', 1),
(3, 0, 'cat', 1),
(4, 0, 'cat', 1),
(5, 0, 'asdasds', 0),
(6, 0, 'asdasds', 0),
(7, 0, 'asdasds', 0),
(8, 0, 'asdasdasdad', 0),
(9, 0, 'adas', 0),
(10, 0, 'testing123', 0),
(11, 0, 'asdadasdasdasdas', 0),
(12, 1, 'sab', 1),
(13, 3, 'aaaaa', 1),
(14, 0, 'ptnest', 1),
(15, 30, 'arsalan', 1),
(16, 0, 'usama', 1),
(17, 0, 'dasfdas', 1);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `type_id` int(255) NOT NULL,
  `type_nm` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`type_id`, `type_nm`, `status`) VALUES
(1, 'Books', 1),
(2, 'Article', 1),
(3, 'Research Paper', 1),
(4, 'Journal', 0),
(5, 'new', 0),
(6, 'usama', 0),
(7, 'new', 0),
(8, 'new york', 0),
(9, 'sam', 0),
(10, 'new', 0),
(11, 'novel', 0),
(12, 'novel1', 0),
(13, 'books', 0),
(14, 'books', 0),
(15, 'ononon', 0),
(16, '1111', 0),
(17, 'test1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `u_id` int(4) NOT NULL,
  `user_type_id` int(255) NOT NULL,
  `u_fnm` varchar(35) NOT NULL,
  `u_pwd` varchar(20) NOT NULL,
  `u_gender` varchar(7) NOT NULL,
  `u_email` varchar(35) NOT NULL,
  `u_contact` varchar(12) NOT NULL,
  `u_city` varchar(20) NOT NULL,
  `user_reg_num` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `user_type_id`, `u_fnm`, `u_pwd`, `u_gender`, `u_email`, `u_contact`, `u_city`, `user_reg_num`, `status`) VALUES
(4, 1, 'Muhammad Junaid', 'admin123', 'male', 'admin@gmail.com', '9859632561', 'Rajkot', '', 1),
(6, 2, 'junaid420', 'asd123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 1),
(7, 2, 'arsalan ', 'asd123', 'male', 'arsalan@gmail.com', '9859632561', 'Rajkot', '', 0),
(8, 2, 'junaid420', '123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 0),
(9, 2, 'junaid420', 'ASD123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 0),
(10, 2, 'junaid420', 'asd123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 0),
(11, 2, 'junaid420', 'asd123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 0),
(12, 2, 'junaid420', 'asd123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 0),
(13, 2, 'junaid420', 'asd123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 0),
(14, 2, 'junaid420', 'asd123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 0),
(15, 2, 'junaid420', 'asd123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 0),
(16, 2, 'junaid420', 'asd123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 0),
(17, 3, 'junaid420', 'asd123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '', 0),
(18, 3, 'junaid420', 'asd123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '2232', 0),
(19, 3, 'junaid', 'asd123', 'male', 'user@gmail.com', '9859632561', 'Rajkot', '6164', 1),
(20, 3, 'junaid420', 'asd', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '5411', 0),
(21, 3, 'junaid420', 'asd', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '8996', 0),
(22, 3, 'junaid420', 'asd', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '7388', 0),
(23, 3, 'junaid420', '123', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '7165', 0),
(24, 3, 'junaid420', 'asd', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '1814', 0),
(25, 3, 'junaid420', 'asd', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '3792', 0),
(26, 3, 'junaid420', 'asd', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '9742', 0),
(27, 3, 'junaid420', 'asd', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '9194', 0),
(28, 3, 'junaid420', 'asd', 'male', 'admin1@gmail.com', '9859632561', 'Rajkot', '5027', 1),
(29, 3, 'hassan', '123', 'Male', 'hassan@ptnest.com', '34234', 'asgfdas', '7633', 1),
(30, 3, 'sheikh', '123', 'Male', 'shah@ptnest.com', '234', 'asdfds', '6247', 1),
(31, 3, 'sana1', '123', 'Male', 'sana@ptnest.com', '3423', 'fkasdf', '7696', 1),
(32, 3, 'salman', '123', 'Male', 'salman@ptnest.com', '3434', 'asfads', '2210', 0),
(33, 3, 'ali', '123', 'Male', 'ali@ptnest.com', '343', 'dasf', '5909', 0),
(34, 0, '\".$fnm.\"', '\".$pwd.\"', '\".$gend', '\".$email.\"', '\".$contact.\"', '\".$city.\"', '\".$random.\"', 1),
(35, 3, 'asdasd', 'asd123', 'Male', 'adsdas@ptnest.com', '923i54239542', 'Ahmedabad', '5784', 0),
(36, 3, 'asdasd', 'asd123', 'Male', 'adsdas@ptnest.com', '923i54239542', 'Ahmedabad', '3363', 0),
(37, 3, 'asif', '123', 'Male', 'asif@ptnest.com', '3423', 'Ahmedabad', '5464', 1),
(38, 3, 'usama', 'asd', 'Male', 'usama@ptnest.com', '213213', 'adsa', '7582', 1),
(39, 3, 'junaid', '123', 'Male', 'admin@ptnest.com', '1233213', 'Mehsana', '7363', 0),
(40, 3, 'usama', '123', 'Male', 'usama@ptnest.com', '123213', 'Ahmedabad', '3116', 0),
(41, 3, 'usama yameen', '123123', 'Male', 'usama@ptnest.com', '123', 'Ahmedabad', '4256', 1),
(42, 3, 'Muhammad Junaid 1', '123', 'Male', 'junaidalishah861@gmail.com', '03460504599', 'karachi', '4088', 1),
(43, 3, 'junaid', '123', 'Male', 'junaid@ptnest.com', '3423', 'karachi', '7392', 0),
(44, 3, 'junaid', '123', 'Male', 'junaid@ptnest.com', '3423', 'karachi', '3888', 0),
(45, 3, 'junaid', '123', 'Male', 'junaid33@ptnest.com', '434534', 'lkfd', '7044', 0),
(46, 3, 'arsalan', '123', 'Male', 'arsalan@ptnest.com', '453534', 'kfjdasfkljasl', '1425', 0),
(47, 3, 'junaid', '123', 'Male', 'junaid@ptnest.com', '8', 'fgdfg', '3111', 0),
(48, 3, 'junaid', 'aaa', 'Male', 'junaid@ptnest.com', '3243', 'dsfdas', '5390', 0),
(49, 3, 'junaid', 'aaa', 'Male', 'junaid@ptnest.com', '3243', 'karachi', '6113', 0),
(50, 3, 'junaid', 'aaa', 'Male', 'junaid@ptnest.com', '3243', 'karachi', '9941', 0),
(51, 3, 'junaid', 'aaa', 'Male', 'junaid@ptnest.com', '3243', 'karachi', '8000', 0),
(52, 3, 'dsf', 'asd', 'Male', 'adsf@ptnest.com', '34', 'fjasdkl', '3613', 0),
(53, 3, 'dsf', 'asd', 'Male', 'adsf@ptnest.com', '34', 'fjasdkl', '9539', 0),
(54, 3, 'asdfs', 'asd', 'Male', 'asdfasd@ptnest.com', '324234', 'adsfdas', '8141', 0),
(55, 3, 'asdfs', 'asd', 'Male', 'asdfasd@ptnest.com', '324234', 'adsfdas', '2967', 0),
(56, 3, 'usama yameen', 'asd', 'Male', 'usama@ptnest.com', '54646546', 'asd', '5698', 0),
(57, 3, 'fddas', 'asdz', 'Male', 'asdfas@ptnest.com', 'afsd', 'sfas', '7788', 0),
(58, 3, 'fddas', 'asdz', 'Male', 'asdfas@ptnest.com', 'afsd', 'sfas', '2370', 0),
(59, 3, 'asdfdas', 'asd', 'Male', 'junaidalishah86@gmail.com', '43', 'afds', '4661', 0),
(60, 3, 'asdfdas', 'asd', 'Male', 'junaidalishah86@gmail.com', '43', 'afds', '3372', 0),
(61, 3, 'dsaf', 'asd', 'Male', 'junaidalishah86@gmail.com', '43', 'asdfds', '7468', 0),
(62, 3, 'dsaf', 'asd', 'Male', 'junaidalishah86@gmail.com', '43', 'asdfds', '5446', 0),
(63, 3, 'dsaf', 'asd', 'Male', 'junaidalishah86@gmail.com', '43', 'asdfds', '1264', 0),
(64, 3, 'dsaf', 'asd', 'Male', 'junaidalishah86@gmail.com', '43', 'asdfds', '1037', 0),
(65, 3, 'dsaf', 'asd', 'Male', 'junaidalishah86@gmail.com', '43', 'asdfds', '7667', 0),
(66, 3, 'asd', 'asd', 'Male', 'asd@ptnest.com', 'asd', 'asd', '6011', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `user_type_id` int(255) NOT NULL,
  `user_type_nm` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`user_type_id`, `user_type_nm`, `status`) VALUES
(1, 'Admin', 1),
(2, 'Employee', 1),
(3, 'User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `variables`
--

CREATE TABLE `variables` (
  `id` int(20) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `type` enum('dependent','independent','mediating','moderating') NOT NULL,
  `status` text NOT NULL,
  `article_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `instrument` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `variables`
--

INSERT INTO `variables` (`id`, `name`, `description`, `type`, `status`, `article_id`, `created_date`, `instrument`) VALUES
(1, 'Varible2', 'Variable description', 'independent', '1', 1, '2018-11-22 11:30:56', ''),
(2, 'Varible2', 'Varible2 Descripton', 'dependent', '1', 1, '2018-11-22 16:56:09', ''),
(3, 'Varible2', '', 'mediating', '1', 2390, '2018-11-22 16:56:32', ''),
(13626, 'Servant_leadership', 'description121', 'dependent', '1', 2406, '2019-01-17 01:47:27', ''),
(13627, 'Customer_orientation', 'description121', 'dependent', '1', 2406, '2019-01-17 01:47:27', ''),
(13628, 'Turnover_intention', 'description121', 'dependent', '1', 2406, '2019-01-17 01:47:27', ''),
(13629, 'Servant_leadership', 'description121', 'dependent', '1', 2407, '2019-01-17 01:47:27', ''),
(13630, 'Organizational_citizenship_behavior', 'description121', 'dependent', '1', 2407, '2019-01-17 01:47:27', ''),
(13631, 'Servant_leadership', 'description121', 'dependent', '1', 2408, '2019-01-17 01:47:27', ''),
(13632, 'Organizational_citizenship_behavior', 'description121', 'dependent', '1', 2408, '2019-01-17 01:47:27', ''),
(13633, 'Cynicism', 'description121', 'dependent', '1', 2408, '2019-01-17 01:47:27', ''),
(13634, 'Organizational_Commitment', 'description121', 'dependent', '1', 2408, '2019-01-17 01:47:27', ''),
(13635, 'Leadership_Integrity', 'description121', 'dependent', '1', 2408, '2019-01-17 01:47:27', ''),
(13636, 'Servant_leadership', 'description121', 'dependent', '1', 2409, '2019-01-17 01:47:27', ''),
(13637, 'Job_satisfaction', 'description121', 'dependent', '1', 2409, '2019-01-17 01:47:27', ''),
(13638, 'Servant_leadership', 'description121', 'dependent', '1', 2410, '2019-01-17 01:47:27', ''),
(13639, 'Organizational_citizenship_behavior', 'description121', 'dependent', '1', 2410, '2019-01-17 01:47:27', ''),
(13640, 'Task_Performance', 'description121', 'dependent', '1', 2410, '2019-01-17 01:47:27', ''),
(13641, 'Servant_leadership', 'description121', 'dependent', '1', 2411, '2019-01-17 01:47:27', ''),
(13642, 'Transformational_leadership', 'description121', 'dependent', '1', 2411, '2019-01-17 01:47:27', ''),
(13643, 'Organizational_Commitment', 'description121', 'dependent', '1', 2411, '2019-01-17 01:47:27', ''),
(13644, 'Job_satisfaction', 'description121', 'dependent', '1', 2411, '2019-01-17 01:47:27', ''),
(13645, 'Intention_to_stay', 'description121', 'dependent', '1', 2411, '2019-01-17 01:47:27', ''),
(13646, 'Servant_leadership', 'description121', 'dependent', '1', 2412, '2019-01-17 01:47:27', ''),
(13647, 'Work_family_conflict', 'description121', 'dependent', '1', 2412, '2019-01-17 01:47:27', ''),
(13648, 'Work_family positive_spillover', 'description121', 'dependent', '1', 2412, '2019-01-17 01:47:27', ''),
(13649, 'Servant_leadership', 'description121', 'dependent', '1', 2413, '2019-01-17 01:47:27', ''),
(13650, 'Turnover_Intention', 'description121', 'dependent', '1', 2413, '2019-01-17 01:47:27', ''),
(13651, 'Organizational_Performance', 'description121', 'dependent', '1', 2413, '2019-01-17 01:47:27', ''),
(13652, 'In_Role_Performance', 'description121', 'dependent', '1', 2413, '2019-01-17 01:47:27', ''),
(13653, 'Creativity', 'description121', 'dependent', '1', 2413, '2019-01-17 01:47:27', ''),
(13654, 'Customer_service_behavior', 'description121', 'dependent', '1', 2413, '2019-01-17 01:47:27', ''),
(13655, 'Servant_leadership', 'description121', 'dependent', '1', 2414, '2019-01-17 01:47:27', ''),
(13656, 'Job_satisfaction', 'description121', 'dependent', '1', 2414, '2019-01-17 01:47:27', ''),
(13657, 'Servant_leadership', 'description121', 'dependent', '1', 2415, '2019-01-17 01:47:27', ''),
(13658, 'Transformational_leadership', 'description121', 'dependent', '1', 2415, '2019-01-17 01:47:27', ''),
(13659, 'Organizational_Performance', 'description121', 'dependent', '1', 2415, '2019-01-17 01:47:27', ''),
(13660, 'Servant_leadership', 'description121', 'dependent', '1', 2416, '2019-01-17 01:47:27', ''),
(13661, 'Organizational_citizenship_behavior', 'description121', 'dependent', '1', 2416, '2019-01-17 01:47:27', ''),
(13662, 'Servant_leadership', 'description121', 'dependent', '1', 2417, '2019-01-17 01:47:27', ''),
(13663, 'Organizational_citizenship_behavior', 'description121', 'dependent', '1', 2417, '2019-01-17 01:47:27', ''),
(13664, 'Job_satisfaction', 'description121', 'dependent', '1', 2417, '2019-01-17 01:47:27', ''),
(13665, 'Servant_leadership', 'description121', 'dependent', '1', 2418, '2019-01-17 01:47:27', ''),
(13666, 'Creativity', 'description121', 'dependent', '1', 2418, '2019-01-17 01:47:27', ''),
(13667, 'Team_Innovation', 'description121', 'dependent', '1', 2418, '2019-01-17 01:47:27', ''),
(13668, 'Servant_leadership', 'description121', 'dependent', '1', 2419, '2019-01-17 01:47:27', ''),
(13669, 'Contingent_Reward', 'description121', 'dependent', '1', 2419, '2019-01-17 01:47:27', ''),
(13670, 'Commitment_to_Change', 'description121', 'dependent', '1', 2419, '2019-01-17 01:47:27', ''),
(13671, 'Turnover_intention', 'description800', 'dependent', '1', 2406, '2019-01-17 01:47:27', ''),
(13672, 'Servant_leadership', 'description801', 'independent', '1', 2406, '2019-01-17 01:47:27', ''),
(13673, 'Turnover_intention', 'description800', 'dependent', '1', 2406, '2019-01-17 01:47:27', ''),
(13674, 'Customer_orientation', 'description801', 'independent', '1', 2406, '2019-01-17 01:47:27', ''),
(13675, 'Organizational_citizenship_behavior', 'description800', 'dependent', '1', 2407, '2019-01-17 01:47:27', ''),
(13676, 'Servant_leadership', 'description801', 'independent', '1', 2407, '2019-01-17 01:47:27', ''),
(13677, 'Organizational_citizenship_behavior', 'description800', 'dependent', '1', 2408, '2019-01-17 01:47:27', ''),
(13678, 'Servant_leadership', 'description801', 'independent', '1', 2408, '2019-01-17 01:47:27', ''),
(13679, 'Organizational_commitment', 'description800', 'dependent', '1', 2408, '2019-01-17 01:47:27', ''),
(13680, 'Servant_leadership', 'description801', 'independent', '1', 2408, '2019-01-17 01:47:27', ''),
(13681, 'Cynicism', 'description800', 'dependent', '1', 2408, '2019-01-17 01:47:27', ''),
(13682, 'Servant_leadership', 'description801', 'independent', '1', 2408, '2019-01-17 01:47:27', ''),
(13683, 'Leadership_integrity', 'description800', 'dependent', '1', 2408, '2019-01-17 01:47:27', ''),
(13684, 'Servant_leadership', 'description801', 'independent', '1', 2408, '2019-01-17 01:47:27', ''),
(13685, 'Job_satisfaction', 'description800', 'dependent', '1', 2409, '2019-01-17 01:47:27', ''),
(13686, 'Servant_leadership', 'description801', 'independent', '1', 2409, '2019-01-17 01:47:27', ''),
(13687, 'Organizational_citizenship_behavior', 'description800', 'dependent', '1', 2410, '2019-01-17 01:47:27', ''),
(13688, 'Servant_leadership', 'description801', 'independent', '1', 2410, '2019-01-17 01:47:27', ''),
(13689, 'Task_Performance', 'description800', 'dependent', '1', 2410, '2019-01-17 01:47:27', ''),
(13690, 'Servant_leadership', 'description801', 'independent', '1', 2410, '2019-01-17 01:47:28', ''),
(13691, 'Organizational_Commitment', 'description800', 'dependent', '1', 2411, '2019-01-17 01:47:28', ''),
(13692, 'Servant_leadership', 'description801', 'independent', '1', 2411, '2019-01-17 01:47:28', ''),
(13693, 'Job_satisfaction', 'description800', 'dependent', '1', 2411, '2019-01-17 01:47:28', ''),
(13694, 'Transformational_leadership', 'description801', 'independent', '1', 2411, '2019-01-17 01:47:28', ''),
(13695, 'Intention_to_stay', 'description800', 'dependent', '1', 2411, '2019-01-17 01:47:28', ''),
(13696, 'Servant_leadership', 'description801', 'independent', '1', 2411, '2019-01-17 01:47:28', ''),
(13697, 'Work_family_conflict', 'description800', 'dependent', '1', 2412, '2019-01-17 01:47:28', ''),
(13698, 'Servant_leadership', 'description801', 'independent', '1', 2412, '2019-01-17 01:47:28', ''),
(13699, 'Work_family_positive_spillover', 'description800', 'dependent', '1', 2412, '2019-01-17 01:47:28', ''),
(13700, 'Servant_leadership', 'description801', 'independent', '1', 2412, '2019-01-17 01:47:28', ''),
(13701, 'Turnover_Intention', 'description800', 'dependent', '1', 2413, '2019-01-17 01:47:28', ''),
(13702, 'Servant_leadership', 'description801', 'independent', '1', 2413, '2019-01-17 01:47:28', ''),
(13703, 'Organizational_Performance', 'description800', 'dependent', '1', 2413, '2019-01-17 01:47:28', ''),
(13704, 'Servant_leadership', 'description801', 'independent', '1', 2413, '2019-01-17 01:47:28', ''),
(13705, 'In_Role_Performance', 'description800', 'dependent', '1', 2413, '2019-01-17 01:47:28', ''),
(13706, 'Servant_leadership', 'description801', 'independent', '1', 2413, '2019-01-17 01:47:28', ''),
(13707, 'Creativity', 'description800', 'dependent', '1', 2413, '2019-01-17 01:47:28', ''),
(13708, 'Servant_leadership', 'description801', 'independent', '1', 2413, '2019-01-17 01:47:28', ''),
(13709, 'Customer_service_behavior', 'description800', 'dependent', '1', 2413, '2019-01-17 01:47:28', ''),
(13710, 'Servant_leadership', 'description801', 'independent', '1', 2413, '2019-01-17 01:47:28', ''),
(13711, 'Job_satisfaction', 'description800', 'dependent', '1', 2414, '2019-01-17 01:47:28', ''),
(13712, 'Servant_leadership', 'description801', 'independent', '1', 2414, '2019-01-17 01:47:28', ''),
(13713, 'Transformational_leadership', 'description800', 'dependent', '1', 2415, '2019-01-17 01:47:28', ''),
(13714, 'Servant_leadership', 'description801', 'independent', '1', 2415, '2019-01-17 01:47:28', ''),
(13715, 'Organizational_Performance', 'description800', 'dependent', '1', 2415, '2019-01-17 01:47:28', ''),
(13716, 'Servant_leadership', 'description801', 'independent', '1', 2415, '2019-01-17 01:47:28', ''),
(13717, 'Organizational_citizenship_behavior', 'description800', 'dependent', '1', 2416, '2019-01-17 01:47:28', ''),
(13718, 'Servant_leadership', 'description801', 'independent', '1', 2416, '2019-01-17 01:47:28', ''),
(13719, 'Job_satisfaction', 'description800', 'dependent', '1', 2417, '2019-01-17 01:47:28', ''),
(13720, 'Servant_leadership', 'description801', 'independent', '1', 2417, '2019-01-17 01:47:28', ''),
(13721, 'Creativity', 'description800', 'dependent', '1', 2418, '2019-01-17 01:47:28', ''),
(13722, 'Servant_leadership', 'description801', 'independent', '1', 2418, '2019-01-17 01:47:28', ''),
(13723, 'Team_Innovation', 'description800', 'dependent', '1', 2418, '2019-01-17 01:47:28', ''),
(13724, 'Servant_leadership', 'description801', 'independent', '1', 2418, '2019-01-17 01:47:28', ''),
(13725, 'Commitment_to_Change', 'description800', 'dependent', '1', 2419, '2019-01-17 01:47:28', ''),
(13726, 'Servant_leadership', 'description801', 'independent', '1', 2419, '2019-01-17 01:47:28', ''),
(13727, 'Commitment_to_Change', 'description800', 'dependent', '1', 2419, '2019-01-17 01:47:28', ''),
(13728, 'Contingent_Reward', 'description801', 'independent', '1', 2419, '2019-01-17 01:47:28', ''),
(13729, 'Behavioral_Intention', 'description121', 'dependent', '1', 2420, '2019-01-17 01:47:49', ''),
(13730, 'Brand_Credibility', 'description121', 'dependent', '1', 2420, '2019-01-17 01:47:49', ''),
(13731, 'Brand_Purchase_Intention', 'description121', 'dependent', '1', 2421, '2019-01-17 01:47:49', ''),
(13732, 'Behavioral_Intention', 'description800', 'dependent', '1', 2420, '2019-01-17 01:47:49', ''),
(13733, 'Brand_Credibility', 'description801', 'independent', '1', 2420, '2019-01-17 01:47:49', ''),
(13734, 'Brand_Purchase_Intention', 'description800', 'dependent', '1', 2421, '2019-01-17 01:47:49', ''),
(13735, 'Brand_Credibility', 'description801', 'independent', '1', 2421, '2019-01-17 01:47:49', ''),
(13736, 'Behavioral_Intention', 'description121', 'dependent', '1', 2423, '2019-01-17 01:48:10', ''),
(13737, 'Brand_Credibility', 'description121', 'dependent', '1', 2423, '2019-01-17 01:48:10', ''),
(13738, 'Brand_Purchase_Intention', 'description121', 'dependent', '1', 2424, '2019-01-17 01:48:10', ''),
(13739, 'Brand_Credibility', 'description121', 'dependent', '1', 2424, '2019-01-17 01:48:10', ''),
(13740, 'Brand_Purchase_Intention', 'description121', 'dependent', '1', 2425, '2019-01-17 01:48:10', ''),
(13741, 'Brand_Credibility', 'description121', 'dependent', '1', 2425, '2019-01-17 01:48:10', ''),
(13742, 'Brand_Prestige', 'description121', 'dependent', '1', 2425, '2019-01-17 01:48:10', ''),
(13743, 'Customer_Loyalty', 'description121', 'dependent', '1', 2426, '2019-01-17 01:48:10', ''),
(13744, 'Perceived_Quality', 'description121', 'dependent', '1', 2426, '2019-01-17 01:48:10', ''),
(13745, 'Trustworthiness', 'description121', 'dependent', '1', 2426, '2019-01-17 01:48:10', ''),
(13746, 'Brand_Purchase_Intention', 'description121', 'dependent', '1', 2427, '2019-01-17 01:48:10', ''),
(13747, 'Brand_Credibility', 'description121', 'dependent', '1', 2427, '2019-01-17 01:48:10', ''),
(13748, 'Brand_Experience', 'description121', 'dependent', '1', 2427, '2019-01-17 01:48:10', ''),
(13749, 'Brand_Loyalty', 'description121', 'dependent', '1', 2428, '2019-01-17 01:48:10', ''),
(13750, 'Advertisement_Spending', 'description121', 'dependent', '1', 2428, '2019-01-17 01:48:10', ''),
(13751, 'Attitude_towards_Advertisement', 'description121', 'dependent', '1', 2428, '2019-01-17 01:48:10', ''),
(13752, 'Monetary_Promotions', 'description121', 'dependent', '1', 2428, '2019-01-17 01:48:10', ''),
(13753, 'Non-monetary_Promotions', 'description121', 'dependent', '1', 2428, '2019-01-17 01:48:10', ''),
(13754, 'Customer_Loyalty', 'description121', 'dependent', '1', 2429, '2019-01-17 01:48:10', ''),
(13755, 'Brand_Innovativeness', 'description121', 'dependent', '1', 2429, '2019-01-17 01:48:10', ''),
(13756, 'Consumer_Price_Sensitivity', 'description121', 'dependent', '1', 2430, '2019-01-17 01:48:10', ''),
(13757, 'Brand_Credibility', 'description121', 'dependent', '1', 2430, '2019-01-17 01:48:10', ''),
(13758, 'Information_Cost_Saved', 'description121', 'dependent', '1', 2431, '2019-01-17 01:48:10', ''),
(13759, 'Perceived_Risk', 'description121', 'dependent', '1', 2431, '2019-01-17 01:48:10', ''),
(13760, 'Perceived_Quality', 'description121', 'dependent', '1', 2431, '2019-01-17 01:48:10', ''),
(13761, 'Brand_Credibility', 'description121', 'dependent', '1', 2431, '2019-01-17 01:48:10', ''),
(13762, 'Brand_Purchase_Intention', 'description121', 'dependent', '1', 2432, '2019-01-17 01:48:10', ''),
(13763, 'Brand_Personality', 'description121', 'dependent', '1', 2432, '2019-01-17 01:48:10', ''),
(13764, 'Product_Consideration_Purchase', 'description121', 'dependent', '1', 2433, '2019-01-17 01:48:10', ''),
(13765, 'Brand_Credibility', 'description121', 'dependent', '1', 2433, '2019-01-17 01:48:10', ''),
(13766, 'Word_of_Mouth', 'description121', 'dependent', '1', 2434, '2019-01-17 01:48:10', ''),
(13767, 'Brand_Credibility', 'description121', 'dependent', '1', 2434, '2019-01-17 01:48:10', ''),
(13768, 'Switching_Intention', 'description121', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13769, 'Loyalty_Intention', 'description121', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13770, 'Core_Quality', 'description121', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13771, 'Relational_Quality', 'description121', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13772, 'Perceived_Value', 'description121', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13773, 'Repurchase_Intention', 'description121', 'dependent', '1', 2436, '2019-01-17 01:48:10', ''),
(13774, 'Perceived_Quality', 'description121', 'dependent', '1', 2436, '2019-01-17 01:48:10', ''),
(13775, 'Behavioral_Loyalty', 'description121', 'dependent', '1', 2437, '2019-01-17 01:48:10', ''),
(13776, 'Utilitarian_Value', 'description121', 'dependent', '1', 2437, '2019-01-17 01:48:10', ''),
(13777, 'Hedonic_Value', 'description121', 'dependent', '1', 2437, '2019-01-17 01:48:10', ''),
(13778, 'Social_Value', 'description121', 'dependent', '1', 2437, '2019-01-17 01:48:10', ''),
(13779, 'Behavioral_Intention', 'description800', 'dependent', '1', 2423, '2019-01-17 01:48:10', ''),
(13780, 'Brand_Credibility', 'description801', 'independent', '1', 2423, '2019-01-17 01:48:10', ''),
(13781, 'Brand_Purchase_Intention', 'description800', 'dependent', '1', 2424, '2019-01-17 01:48:10', ''),
(13782, 'Brand_Credibility', 'description801', 'independent', '1', 2424, '2019-01-17 01:48:10', ''),
(13783, 'Brand_Purchase_Intention', 'description800', 'dependent', '1', 2425, '2019-01-17 01:48:10', ''),
(13784, 'Brand_Credibility', 'description801', 'independent', '1', 2425, '2019-01-17 01:48:10', ''),
(13785, 'Brand_Purchase_Intention', 'description800', 'dependent', '1', 2425, '2019-01-17 01:48:10', ''),
(13786, 'Brand_Prestige', 'description801', 'independent', '1', 2425, '2019-01-17 01:48:10', ''),
(13787, 'Customer_Loyalty', 'description800', 'dependent', '1', 2426, '2019-01-17 01:48:10', ''),
(13788, 'Perceived_Quality', 'description801', 'independent', '1', 2426, '2019-01-17 01:48:10', ''),
(13789, 'Customer_Loyalty', 'description800', 'dependent', '1', 2426, '2019-01-17 01:48:10', ''),
(13790, 'Trustworthiness', 'description801', 'independent', '1', 2426, '2019-01-17 01:48:10', ''),
(13791, 'Brand_Purchase_Intention', 'description800', 'dependent', '1', 2427, '2019-01-17 01:48:10', ''),
(13792, 'Brand_Credibility', 'description801', 'independent', '1', 2427, '2019-01-17 01:48:10', ''),
(13793, 'Brand_Purchase_Intention', 'description800', 'dependent', '1', 2427, '2019-01-17 01:48:10', ''),
(13794, 'Brand_Experience', 'description801', 'independent', '1', 2427, '2019-01-17 01:48:10', ''),
(13795, 'Brand_Loyalty', 'description800', 'dependent', '1', 2428, '2019-01-17 01:48:10', ''),
(13796, 'Advertisement_Spending', 'description801', 'independent', '1', 2428, '2019-01-17 01:48:10', ''),
(13797, 'Brand_Loyalty', 'description800', 'dependent', '1', 2428, '2019-01-17 01:48:10', ''),
(13798, 'Attitude_towards_Advertisement', 'description801', 'independent', '1', 2428, '2019-01-17 01:48:10', ''),
(13799, 'Brand_Loyalty', 'description800', 'dependent', '1', 2428, '2019-01-17 01:48:10', ''),
(13800, 'Monetary_Promotions', 'description801', 'independent', '1', 2428, '2019-01-17 01:48:10', ''),
(13801, 'Brand_Loyalty', 'description800', 'dependent', '1', 2428, '2019-01-17 01:48:10', ''),
(13802, 'Non-monetary_Promotions', 'description801', 'independent', '1', 2428, '2019-01-17 01:48:10', ''),
(13803, 'Customer_Loyalty', 'description800', 'dependent', '1', 2429, '2019-01-17 01:48:10', ''),
(13804, 'Brand_Innovativeness', 'description801', 'independent', '1', 2429, '2019-01-17 01:48:10', ''),
(13805, 'Consumer_Price_Sensitivity', 'description800', 'dependent', '1', 2430, '2019-01-17 01:48:10', ''),
(13806, 'Brand_Credibility', 'description801', 'independent', '1', 2430, '2019-01-17 01:48:10', ''),
(13807, 'Information_Cost_Saved', 'description800', 'dependent', '1', 2431, '2019-01-17 01:48:10', ''),
(13808, 'Brand_Credibility', 'description801', 'independent', '1', 2431, '2019-01-17 01:48:10', ''),
(13809, 'Perceived_Risk', 'description800', 'dependent', '1', 2431, '2019-01-17 01:48:10', ''),
(13810, 'Brand_Credibility', 'description801', 'independent', '1', 2431, '2019-01-17 01:48:10', ''),
(13811, 'Perceived_Quality', 'description800', 'dependent', '1', 2431, '2019-01-17 01:48:10', ''),
(13812, 'Brand_Credibility', 'description801', 'independent', '1', 2431, '2019-01-17 01:48:10', ''),
(13813, 'Brand_Purchase_Intention', 'description800', 'dependent', '1', 2432, '2019-01-17 01:48:10', ''),
(13814, 'Brand_Personality', 'description801', 'independent', '1', 2432, '2019-01-17 01:48:10', ''),
(13815, 'Product_Consideration_Purchase', 'description800', 'dependent', '1', 2433, '2019-01-17 01:48:10', ''),
(13816, 'Brand_Credibility', 'description801', 'independent', '1', 2433, '2019-01-17 01:48:10', ''),
(13817, 'Word_of_Mouth', 'description800', 'dependent', '1', 2434, '2019-01-17 01:48:10', ''),
(13818, 'Brand_Credibility', 'description801', 'independent', '1', 2434, '2019-01-17 01:48:10', ''),
(13819, 'Switching_Intention', 'description800', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13820, 'Core_Quality', 'description801', 'independent', '1', 2435, '2019-01-17 01:48:10', ''),
(13821, 'Loyalty_Intention', 'description800', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13822, 'Core_Quality', 'description801', 'independent', '1', 2435, '2019-01-17 01:48:10', ''),
(13823, 'Switching_Intention', 'description800', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13824, 'Relational_Quality', 'description801', 'independent', '1', 2435, '2019-01-17 01:48:10', ''),
(13825, 'Loyalty_Intention', 'description800', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13826, 'Relational_Quality', 'description801', 'independent', '1', 2435, '2019-01-17 01:48:10', ''),
(13827, 'Switching_Intention', 'description800', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13828, 'Perceived_Value', 'description801', 'independent', '1', 2435, '2019-01-17 01:48:10', ''),
(13829, 'Loyalty_Intention', 'description800', 'dependent', '1', 2435, '2019-01-17 01:48:10', ''),
(13830, 'Perceived_Value', 'description801', 'independent', '1', 2435, '2019-01-17 01:48:10', ''),
(13831, 'Repurchase_Intention', 'description800', 'dependent', '1', 2436, '2019-01-17 01:48:10', ''),
(13832, 'Perceived_Quality', 'description801', 'independent', '1', 2436, '2019-01-17 01:48:10', ''),
(13833, 'Behavioral_Loyalty', 'description800', 'dependent', '1', 2437, '2019-01-17 01:48:10', ''),
(13834, 'Utilitarian_Value', 'description801', 'independent', '1', 2437, '2019-01-17 01:48:10', ''),
(13835, 'Behavioral_Loyalty', 'description800', 'dependent', '1', 2437, '2019-01-17 01:48:10', ''),
(13836, 'Hedonic_Value', 'description801', 'independent', '1', 2437, '2019-01-17 01:48:10', ''),
(13837, 'Behavioral_Loyalty', 'description800', 'dependent', '1', 2437, '2019-01-17 01:48:10', ''),
(13838, 'Social_Value', 'description801', 'independent', '1', 2437, '2019-01-17 01:48:10', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`con_id`);

--
-- Indexes for table `domain`
--
ALTER TABLE `domain`
  ADD PRIMARY KEY (`domain_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `front_user`
--
ALTER TABLE `front_user`
  ADD PRIMARY KEY (`f_user_id`);

--
-- Indexes for table `gallery_album`
--
ALTER TABLE `gallery_album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jornal`
--
ALTER TABLE `jornal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `old_book`
--
ALTER TABLE `old_book`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `owlexport`
--
ALTER TABLE `owlexport`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`rel_id`);

--
-- Indexes for table `spread_sheet_upload`
--
ALTER TABLE `spread_sheet_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcat`
--
ALTER TABLE `subcat`
  ADD PRIMARY KEY (`subcat_id`);

--
-- Indexes for table `testing`
--
ALTER TABLE `testing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testing_db`
--
ALTER TABLE `testing_db`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`u_id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`user_type_id`);

--
-- Indexes for table `variables`
--
ALTER TABLE `variables`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2438;

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2429;

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `b_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=411;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `con_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `domain`
--
ALTER TABLE `domain`
  MODIFY `domain_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `front_user`
--
ALTER TABLE `front_user`
  MODIFY `f_user_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gallery_album`
--
ALTER TABLE `gallery_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gallery_images`
--
ALTER TABLE `gallery_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jornal`
--
ALTER TABLE `jornal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `old_book`
--
ALTER TABLE `old_book`
  MODIFY `b_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `owlexport`
--
ALTER TABLE `owlexport`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `rel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `spread_sheet_upload`
--
ALTER TABLE `spread_sheet_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `subcat`
--
ALTER TABLE `subcat`
  MODIFY `subcat_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22449;

--
-- AUTO_INCREMENT for table `testing`
--
ALTER TABLE `testing`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testing_db`
--
ALTER TABLE `testing_db`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `type_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `u_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `user_type_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `variables`
--
ALTER TABLE `variables`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13839;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
