<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="library/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="library/css/custom.css">
<!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
<link rel="stylesheet" type="text/css" href="library/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="library/css/jquery-ui.theme.css">
<link rel="stylesheet" type="text/css" href="library/css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="library/css/jquery-ui.structure.css">
<link rel="stylesheet" type="text/css" href="library/css/pick-a-color-1.2.3.min.css">


<script src="library/js/jquery.min.js"></script>

<script src="js/bootstrap.min.js"></script>

<script src="js/jquery-1.11.0.ui.js"></script>

<script src="js/mousetrap.min.js"></script>

<script src="js/select2.min.js"></script>